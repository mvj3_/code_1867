//题目分析:
//我去WA了好多次居然是因为Word的w没大写。
//因为要数字母个数，我用了数组为长度为27的存字母个数了。
//剩下的是判断素数。这题好简单不多说。代码应该看得懂吧

//题目网址:http://soj.me/1765

#include <iostream>
#include <cmath>
#include <string>

using namespace std;

int alp[27] = {0};

bool isPrime(int p)
{
    if (p == 1 || p == 0) {
        return false;
    } else {
        for (int i = 2; i <= sqrt(double(p)); i++) {
            if (p%i == 0)
                return false;
        }
    }
    return true;
}

int main()
{
    string word;
    int max, min;
    while (cin >> word) {
        max = 0;
        min = 999;
        for (int i = 0; i < word.length(); i++) {
            alp[word[i]-'a'+1]++;
        }

        for (int j = 1; j < 27; j++) {
            if (alp[j] != 0) {
                if (alp[j] < min) {
                    min = alp[j];
                } else if (alp[j] > max) {
                    max = alp[j];
                }
            }
            alp[j] = 0;
        }

        if (isPrime(max - min)) {
             cout << "Lucky Word" << endl
                  << max - min << endl;
        } else {
            cout << "No Answer" << endl
                 << "0" << endl;
        }
    }
    
    return 0;
}                                 